"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
let router = express_1.default.Router();
router.get("/users", (req, res, next) => {
    res.send("Users Route");
});
router.post("/users", (req, res, next) => {
    res.json(req.body);
});
exports.default = router;
