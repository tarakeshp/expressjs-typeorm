import express from "express";
import { UserLoginRequest, IRequest, IResponse } from "../interfaces";

let router = express.Router();

router.get("/users", (req: IRequest, res: IResponse, next) => {
  res.send("Users Route");
});

router.post("/users", (req: IRequest, res: IResponse, next) => {
  res.json(req.body as UserLoginRequest);
});

export default router;
