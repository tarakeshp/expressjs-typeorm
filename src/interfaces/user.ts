import { IRequest } from "./express";

export interface UserLoginRequest extends IRequest {
  username?: string;
  password?: string;
}
