import { Request, Response, NextFunction } from "express";

export interface IRequest extends Request {
  Session?: any;
  User?: any;
}
export interface IResponse extends Response {}
export interface INext extends NextFunction {}
