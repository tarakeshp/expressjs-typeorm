import express from "express";
import bodyParser from "body-parser";
import Routes from "./routes";
import { IRequest, IResponse } from "./interfaces";

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req: IRequest, res: IResponse) => {
  res.send("Hello World");
});

app.use("/api", Routes.UserRouter);

export default app;
